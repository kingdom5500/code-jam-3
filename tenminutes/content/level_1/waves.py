# Stdlib
from typing import List

# CJ3 Internals
from tenminutes.content.level_1.enemies import Monster, Dragon, Skeleton
from tenminutes.game.wave import Wave


def get_waves(containers) -> List[Wave]:
    return [
        Wave(containers, [Monster, Skeleton, Dragon]),
        Wave(containers, [Monster, Dragon])
    ]
