# Stdlib
import random
from typing import List, Type

# CJ3 Internals
from tenminutes.game.entity import EnemyEntity


class Wave:
    # Don't use type hint for containers here for now, it can be RendersUpdate,
    # pygame.Group or a tuple of both types
    def __init__(self, containers, enemies: List[Type[EnemyEntity]],
                 weights: List[int] = None):
        self.containers = containers
        self.enemies = enemies
        self.weights = weights or [1] * len(self.enemies)

    def random_enemy(self):
        enemy_cls = random.choices(self.enemies, weights=self.weights)[0]
        return enemy_cls(self.containers)
