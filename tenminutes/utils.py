from functools import lru_cache

import pygame


@lru_cache(maxsize=100)
def load_image(filename):
    return pygame.image.load(filename)
