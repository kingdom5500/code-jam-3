# External Libraries
import pygame

# CJ3 Internals
from tenminutes.game.scene import Scene


# i've kept this here for now, but i don't know if it's going to be used
class InputHandler:
    def run(self, scene: Scene):
        while scene.playing:
            events = []

            for event in pygame.event.get():
                quit_ = False

                if event.type == pygame.QUIT or event.key == pygame.K_ESCAPE:
                    quit_ = True

                if quit_:
                    scene.playing = False
                else:
                    events.append(event)

        scene.process_input(events, pygame.key.get_pressed())
        scene.update()
        scene.render()
