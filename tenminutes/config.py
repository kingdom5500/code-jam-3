# Stdlib
import json
from os import path

# CJ3 Internals
from tenminutes.content.constants import PROJECT_ROOT


def get_config() -> dict:
    with open(path.join(PROJECT_ROOT, "config.json")) as f:
        return json.load(f)
