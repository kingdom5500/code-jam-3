# Stdlib
from typing import Tuple

# External Libraries
import pygame
from pygame import MOUSEBUTTONDOWN, draw
from pygame.rect import Rect

# CJ3 Internals
from tenminutes.utils import load_image
from tenminutes.content.constants import BLACK, WHITE, ASSET_PATH


class IconButton(pygame.sprite.Sprite):
    def __init__(self, containers, name, x, y, width, callback=None):
        super().__init__(containers)
        size = (width, width)

        icon = load_image(f"{ASSET_PATH}/icons/{name}.png")
        icon = pygame.transform.scale(icon, size)

        img = pygame.Surface(size)
        img.fill(pygame.Color("white"))
        img.blit(icon, (0, 0))
        self.image_normal = img

        img = pygame.Surface(size)
        img.blit(icon, (0, 0))
        img.set_alpha(128)
        img.fill(pygame.Color("aquamarine1"))
        self.image_down = img

        self.image = self.image_normal
        self.rect = self.image.get_rect(topleft=(x, y))

        self.callback = callback or (lambda p: p)
        self.button_down = False

    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            if self.rect.collidepoint(event.pos):
                self.image = self.image_down
                self.button_down = True
        elif event.type == pygame.MOUSEBUTTONUP:
            if self.rect.collidepoint(event.pos) and self.button_down:
                self.callback()
                self.image = self.image_normal
            self.button_down = False


class SpawnUI:
    def __init__(self, level):
        # TODO: Draw UI
        self.level = level
        self.grid_step = 42
        self.grid_x, self.grid_y = 1112, 552
        self.icon_width = 32
        self.buttons = [
            IconButton(
                self.level.render_updates, "sol_icon", self.grid_x + 5,
                self.grid_y + 5, self.icon_width
            ),
            IconButton(
                self.level.render_updates, "navy_icon",
                self.grid_x + 5 + self.grid_step, self.grid_y + 5,
                self.icon_width
            ),
            IconButton(
                self.level.render_updates, "ninja_icon",
                self.grid_x + 5 + self.grid_step * 2, self.grid_y + 5,
                self.icon_width
            ),
            IconButton(
                self.level.render_updates, "us_icon",
                self.grid_x + 5 + self.grid_step * 3, self.grid_y + 5,
                self.icon_width
            ),
        ]
        self._buttons_pos = {(0, 0), (1, 0), (2, 0), (3, 0)}

    @property
    def screen(self):
        return self.level.screen

    @property
    def entities(self):
        return self.level.spawns

    def render(self):
        rect = Rect(self.grid_x, self.grid_y, 168, 168)
        draw.rect(self.screen, BLACK, rect)

        for x in range(4):
            for y in range(1, 4):
                x_ = x * self.grid_step + self.grid_x + 5
                y_ = y * self.grid_step + self.grid_y + 5
                draw.rect(self.screen, WHITE,
                          Rect(x_, y_, self.icon_width, self.icon_width))

        # to remove later when there's no manual rect drawing above
        for button in self.buttons:
            self.screen.blit(button.image, button.rect)

    def spawn_ally(self, position: Tuple[int]) -> None:
        x = (position[0] - self.grid_x) // self.grid_step
        y = (position[1] - self.grid_y) // self.grid_step

        x_in_bounds = 0 <= x < len(self.entities[0])
        y_in_bounds = 0 <= y < len(self.entities)

        if not x_in_bounds or not y_in_bounds:
            return

        entity_cls = self.entities[y][x]
        print(entity_cls)
        if self.level.phase == y + 1:
            self.level.spawn_ally(entity_cls)

    def process_input(self, events):
        for event in events:
            if event.type == MOUSEBUTTONDOWN:
                self.spawn_ally(event.pos)
