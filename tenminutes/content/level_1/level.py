# CJ3 Internals
from tenminutes.content.level_1.waves import get_waves
from tenminutes.game.game import Game
from tenminutes.game.level import Level


class LevelOne(Level):

    def __init__(self, game: Game):
        super().__init__(game)
        self.waves = get_waves((self.render_updates, self.enemies))
