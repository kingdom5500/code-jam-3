# Stdlib
import sys

# External Libraries
from PIL import Image

placeholder_img = Image.new("RGBA", (32, 32))

default = {
    "": placeholder_img
}


def from_set(filename: str) -> dict:
    img = Image.open(f"{filename}.png")
    upscale = [x*2 for x in img.size]
    img = img.resize(upscale)

    with open(f"{filename}.map") as f:
        lines = [line.strip() for line in f.readlines()]

    data = default

    for j, line in enumerate(lines):
        for i, char in enumerate(line.split()):
            if char != "00" and char in data:
                raise Exception(f"{char} is already registered for this tileset")
            sprite = img.crop((i*32, j*32, (i+1)*32, (j+1)*32))
            data[char] = sprite

    return data


def from_data(filename: str):
    with open(f"{filename}.map") as f:
        data = [line.strip() for line in f.readlines()]

    size = (32, 32)
    img = Image.new("RGBA", size, "#000000")
    tileset = default

    i = 0

    for line in data:
        if line.startswith("size"):
            size = tuple(map(int, line[5:].split()))
            img = Image.new("RGBA", size, "#000000")

        elif line.startswith("tileset"):
            tileset = from_set(line.split(" ")[1])

        elif line.startswith("background"):
            background = tileset.get(line.split(" ")[1])
            for x in range(0, img.size[0], 32):
                for y in range(0, img.size[1], 32):
                    img.paste(background, (x, y))

        else:
            for j, chars in enumerate(line.split()):
                for char in chars.split(","):
                    over = Image.new("RGBA", size)
                    over.paste(tileset[char.strip("_")], (j * 32, i * 32))
                    img = Image.alpha_composite(img, over)
            i += 1

    img.save(f"{filename}.png")


if __name__ == '__main__':
    files = sys.argv[1:]
    for file in files:
        from_data(file)
