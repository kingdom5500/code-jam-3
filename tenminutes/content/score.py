import pygame


class Score(pygame.sprite.Sprite):
    def __init__(self, containers):
        super().__init__(containers)
        self.font = pygame.font.SysFont("Comic Sans MS", 40)
        self.color = pygame.Color('white')

        self.score = 0
        self.image = self._get_image()
        self.rect = self.image.get_rect().move(20, 640)

    def _get_image(self):
        return self.font.render(f"Score: {self.score}", 0, self.color)

    def update(self, screen_rect):
        self.image = self._get_image()


class Money(pygame.sprite.Sprite):
    def __init__(self, containers, amount=2000):
        super().__init__(containers)
        self.font = pygame.font.SysFont("Comic Sans MS", 40)
        self.color = pygame.Color('yellow')

        self.amount = amount
        self.image = self._get_image()
        self.rect = self.image.get_rect().move(20, 30)

    def _get_image(self):
        return self.font.render(f"$ {self.amount}", 0, self.color)

    def update(self, screen_rect):
        self.image = self._get_image()
