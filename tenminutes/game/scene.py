# Stdlib
from abc import ABC, abstractmethod

# External Libraries
import pygame

# CJ3 Internals
from tenminutes.utils import load_image
from tenminutes.content.constants import ASSET_PATH, WHITE


class Scene(ABC):
    """A base class for all game scenes."""
    def __init__(self, game):
        self.game = game
        # the game loop should check this attribute before
        # attempting to process the scene, and should pop
        # this scene off the stack if it's set to False
        self.is_active = True

    @abstractmethod
    def init(self):
        """"""

    @abstractmethod
    def process_input(self, events, pressed_keys, screen_rect) -> None:
        """"""

    @abstractmethod
    def update(self, screen_rect: pygame.Rect):
        """"""

    @abstractmethod
    def render(self):
        """"""

    @property
    def screen(self):
        return self.game.screen


class Button(pygame.sprite.Sprite):
    def __init__(self, x, y, width, height, callback, text="",
                 text_color=WHITE):
        super().__init__()
        self.size = (width, height)
        self.image_normal = self._get_button_image('dodgerblue1')
        self.image_hover = self._get_button_image('lightskyblue')
        self.image_down = self._get_button_image('aquamarine1')

        self.image = self.image_normal
        self.rect = self.image.get_rect(topleft=(x, y))

        self._add_text_to_images(text, text_color)

        self.callback = callback
        self.button_down = False

    def _add_text_to_images(self, text, text_color):
        center = self.image.get_rect().center
        font = pygame.font.SysFont("Comic Sans MS", 50)
        text_surf = font.render(text, True, text_color)
        text_rect = text_surf.get_rect(center=center)
        for img in (self.image_normal, self.image_down, self.image_hover):
            img.blit(text_surf, text_rect)

    def _get_button_image(self, color):
        img = pygame.Surface(self.size)
        img.set_alpha(128)
        img.fill(pygame.Color(color))
        return img

    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            if self.rect.collidepoint(event.pos):
                self.image = self.image_down
                self.button_down = True
        elif event.type == pygame.MOUSEBUTTONUP:
            if self.rect.collidepoint(event.pos) and self.button_down:
                self.callback()
                self.image = self.image_hover
            self.button_down = False
        elif event.type == pygame.MOUSEMOTION:
            collided = self.rect.collidepoint(event.pos)
            if collided and not self.button_down:
                self.image = self.image_hover
            elif not collided:
                self.image = self.image_normal


class MainMenuScene(Scene):

    PADDING = 100  # the distance between buttons

    def __init__(self, game):
        super().__init__(game)
        self.image = load_image(f"{ASSET_PATH}/mainmenu/background.png")

        self.all_buttons = pygame.sprite.Group()
        self.play_button: Button = None
        self.quit_button: Button = None

        self.levels = []

    def init(self):
        from tenminutes.content.level_1 import LevelOne
        self.levels.append(LevelOne)
        x = height = self.PADDING
        width = self.screen.get_width() - self.PADDING * 2
        self.play_button = Button(
            x, self.PADDING, width, height, self.play, text="P L A Y"
        )
        self.quit_button = Button(
            x, self.PADDING * 3, width, height, self.quit, text="Q U I T"
        )
        self.all_buttons.add(self.play_button, self.quit_button)

    def play(self):
        level = self.levels.pop()
        self.game.add_scene(level)
        self.is_active = False

    def quit(self):
        self.is_active = False

    @property
    def screen(self):
        return self.game.screen

    def process_input(self, events, pressed_keys, _):
        for event in events:
            for button in self.all_buttons:
                if self.is_active:
                    button.handle_event(event)
                else:
                    return

    def update(self, _):
        self.all_buttons.update()

    def render(self):
        self.screen.blit(self.image, (0, 0))
        self.all_buttons.draw(self.screen)
