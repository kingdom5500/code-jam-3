# Stdlib
from os import path

PROJECT_ROOT = path.abspath(path.join(path.dirname(__file__), "..", ".."))
ASSET_PATH = path.join(PROJECT_ROOT, "tenminutes", "assets")

BLACK = (0, 0, 0)
DARK_GRAY = (48, 48, 48)
WHITE = (255, 255, 255)
RED = (255, 48, 48)
