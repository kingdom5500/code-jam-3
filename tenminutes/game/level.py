# Stdlib
import random
from threading import Lock, Thread
from time import sleep
from typing import List, Type

# External Libraries
import pygame

# CJ3 Internals
from tenminutes.utils import load_image
from tenminutes.content.constants import ASSET_PATH
from tenminutes.content.defaults import (
    Tank, Gunman, Soldier, Warrior, Ninja, Navy
)
from tenminutes.game.entity import SpawnableEntity, Explosion
from tenminutes.game.scene import Scene
from tenminutes.game.spawnui import SpawnUI
from tenminutes.game.wave import Wave
from tenminutes.content.score import Score, Money


class Level(Scene):
    DEFAULT_SPAWNS = [
        [Soldier, Navy, Ninja, Gunman],
        [Warrior, Tank],
        [None, None],
        [None, None]
    ]

    def __init__(self, game, waves: List[Wave] = None,
                 spawns: List[List[SpawnableEntity]] = None):
        super().__init__(game)
        self.name = self.__class__.__name__.lower()
        self.backdrop = load_image(
            f"{ASSET_PATH}/{self.name}/background.png").convert()
        self.foreground = load_image(
            f"{ASSET_PATH}/{self.name}/foreground.png").convert_alpha()

        self.render_updates = pygame.sprite.RenderUpdates()
        self.waves = waves
        self.spawns = spawns or self.DEFAULT_SPAWNS

        self.phases = 4
        self.phase = 1
        self.was_beaten = False

        self.time = 600
        self.lock = Lock()
        self.lock.acquire()
        self.playing = False
        self.paused = False

        self.spawn_ui = SpawnUI(self)

        self.enemies = pygame.sprite.Group()
        self.allies = pygame.sprite.Group()
        self.score = Score(self.render_updates)
        self.money = Money(self.render_updates, 2000)

        self.counter = 600 / self.phase

    def init(self):
        pass

    def spawn_ally(self, entity_cls: Type[SpawnableEntity]):
        e = entity_cls((self.render_updates, self.allies))
        self.money.amount -= e.price

    def spawn_enemy(self):
        random.choice(self.waves).random_enemy()

    @property
    def current_enemies(self) -> List[Wave]:
        return self.waves[:self.phase]

    def load_from_data(self, save: dict):
        # TODO
        pass

    def on_beat_settlement(self):
        if self.phase == self.phases:
            self.phase = 1
            self.finish()
        else:
            self.phase += 1
            self.next_phase()

    def next_phase(self):
        # TODO: Shift to next stage
        pass

    def process_input(self, events, pressed_keys, screen_rect):
        self.spawn_ui.process_input(events)

    def update(self, screen_rect):
        self.render_updates.clear(self.screen, self.backdrop)
        self.render_updates.update(screen_rect)

        collisions = pygame.sprite.groupcollide(
            self.allies, self.enemies, False, False
        )
        for ally, enemies in collisions.items():
            for enemy in enemies:
                if random.randint(0, 1):
                    ally.attack(enemy)
                else:
                    enemy.attack(ally)

                if enemy.alive():
                    enemy.weaken()
                else:
                    enemy.kill()
                    Explosion(self.render_updates, enemy)
                    self.score.score += 1
                    self.money.amount += 50
            if ally.alive():
                ally.weaken()
            else:
                ally.kill()
                Explosion(self.render_updates, ally)

        self.counter += self.phase
        if self.counter > 600 / self.phase:
            self.spawn_enemy()
            self.counter = 0

    def timer(self):
        while self.game.running and self.playing and self.time > 0:
            if not self.paused:
                self.time -= 1
            sleep(1)
        self.lock.release()

    def finish(self):
        self.playing = False

    def render(self):
        self.screen.blit(self.backdrop, (0, 0))
        self.render_updates.draw(self.screen)
        self.screen.blit(self.foreground, (0, 0))
        self.spawn_ui.render()

    def start(self):
        if self.handle():
            print("yay")
            # self.game.add_scene(WinScene)
        else:
            print("nay")
            # self.game.add_scene(LoseScene)
        self.is_active = False

    def handle(self):
        self.playing = True
        Thread(target=self.timer).start()
        self.lock.acquire()

        return self.time != 0 and self.game.running
