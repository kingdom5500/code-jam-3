# Stdlib
from typing import List, Tuple

# External Libraries
from pygame.rect import Rect

# CJ3 Internals
from tenminutes.game.entity import BaseEntity, EnemyEntity, SpawnableEntity


class Monster(EnemyEntity):

    def __init__(self, containers):
        super().__init__(containers, 8, "monster/monster_", 10, power=10)


class Dragon(EnemyEntity):

    def __init__(self, containers):
        super().__init__(containers, 7, "drag/drag_", 10, power=20)


class Skeleton(EnemyEntity):

    def __init__(self, containers):
        super().__init__(containers, 8, "skeleton/alien_", 10, power=30)


class Archer(EnemyEntity):
    def __init__(self, containers):
        super().__init__(containers, 5)
        self.range = 20

    def update(self, screen_rect: Rect):
        # TODO: Check if a user is within X distance
        opp: SpawnableEntity = self.get_nearest_opponent()

        if opp is not None and self.distance(opp) < self.range:
            self.fire_arrow(opp)
        else:
            super().update(screen_rect)

    def fire_arrow(self, opponent: SpawnableEntity):
        Arrow(self.power, opponent).spawn([self.rect.x, self.rect.y])

    def get_nearest_opponent(self):
        pass


class Arrow(EnemyEntity):
    def __init__(self, power, opp: BaseEntity):
        super().__init__(power)
        self.target = opp

        self.a = self.b = self.c = self.step = 0

    def get_abc(self):
        """
        Get A, B and C in AX^2 + BX + C that goes through selfpos, otherpos,
        and top
        """
        x1 = self.rect.x
        y1 = self.rect.y

        x3 = self.target.rect.x
        y3 = self.target.rect.y

        dist = (x1 + x3)
        x2 = dist / 2
        y2 = (y1 + y3) * 0.75

        A1 = -x1 ** 2 + x2 ** 2
        B1 = -x1 + x2
        D1 = -y1 + y2
        A2 = -x2 ** 2 + x3 ** 2
        B2 = -x2 + x3
        D2 = -y2 + y3

        Bmul = -(B2 / B1)
        A3 = Bmul * A1 + A2
        D3 = Bmul * D1 + D2

        self.a = D3 / A3
        self.b = (D1 - A1 * self.a) / B1
        self.c = y1 - self.a * x1 ** 2 - self.b * x1

        self.step = dist / (20 * 60)

    def spawn(self, position: List[int]):
        self.rect.x = position[0]
        self.rect.y = position[1]
        self.get_abc()

    def next_pos(self) -> Tuple[int, int]:
        new_x = round(self.rect.x + self.step)
        new_y = round(self.a*new_x**2 + self.b*new_x + self.c)
        return new_x, new_y

    def update(self, screen_rect: Rect):
        self.rect = Rect(*self.next_pos())
