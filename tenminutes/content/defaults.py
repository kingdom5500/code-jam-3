from tenminutes.game.entity import MovingEntity, SpawnableEntity


class Soldier(SpawnableEntity):
    def __init__(self, containers):
        super().__init__(containers, 10, "soldier/sol_", 15, price=100)


class Navy(SpawnableEntity):
    def __init__(self, containers):
        super().__init__(containers, 4, "navy/navy_", 10, price=150)


class Ninja(SpawnableEntity):
    def __init__(self, containers):
        super().__init__(containers, 6, "ninja/ninja_pos", 10, price=200)


class Gunman(SpawnableEntity):
    def __init__(self, containers):
        super().__init__(containers, 6, "us/us_", 10, price=250)


class Warrior(SpawnableEntity):
    def __init__(self, containers):
        super().__init__(containers, 300)


class Tank(SpawnableEntity):
    MAX_HP = 300

    def __init__(self, containers):
        super().__init__(containers, 500)
