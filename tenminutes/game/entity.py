# Stdlib
from functools import partial
from os import path

# External Libraries
import pygame
from pygame.rect import Rect
from pygame.sprite import Sprite

# CJ3 Internals
from tenminutes import utils
from tenminutes.utils import load_image
from tenminutes.content.constants import ASSET_PATH


ENTITY_PATH = f"{ASSET_PATH}/entities/"
GROUND_LEVEL = 475


def _load_image(i, prefix, width):
    filename = f"{ENTITY_PATH}{prefix}{i}.png"
    img = load_image(filename)
    return pygame.transform.scale(img, (width, 120))


class BaseEntity(Sprite):
    MAX_HP = 100

    def __init__(self, containers, img, power: int = 5, initial_x: int = 100):
        super().__init__(containers)
        if isinstance(img, str):
            filename = img if path.exists(img) else f"{ENTITY_PATH}{img}"
            self.image = utils.load_image(filename)
        else:
            self.image = img
        self.initial_x = initial_x
        self.current_hp = self.MAX_HP
        self.power = power
        self.rect = self._get_rect()
        self.speed = None

    def _get_rect(self):
        return self.image.get_rect(midbottom=[self.initial_x, GROUND_LEVEL])

    def alive(self):
        return self.current_hp > 0

    def update(self, screen_rect: Rect):
        self.rect.move_ip(self.speed, 0)
        self.rect = self.rect.clamp(screen_rect)

    def revert_update(self):
        self.rect.move_ip(- self.speed, 0)

    def attack(self, other):
        other.current_hp -= self.power

    def weaken(self):
        pass


class MovingEntity(BaseEntity):
    def __init__(self, containers, n_images, image_name_prefix, speed_factor,
                 power, initial_x):
        self.n_image = n_images
        self.images = list(map(
            partial(_load_image, prefix=image_name_prefix, width=100),
            range(1, 1 + self.n_image)
        ))
        super().__init__(containers, self.images[0], power, initial_x)
        self.speed_factor = speed_factor
        self.count = 0

    def update(self, screen_rect: pygame.Rect):
        super().update(screen_rect)
        ind = self.count // self.speed_factor % self.n_image
        self.image = self.images[ind]
        self.count += 1


class SpawnableEntity(MovingEntity):
    def __init__(self, containers, n_images, image_name_prefix, speed_factor,
                 price):
        super().__init__(
            containers, n_images, image_name_prefix, speed_factor,
            price // 10, initial_x=100
        )
        self.price = price
        self.speed = 1


class EnemyEntity(MovingEntity):

    def __init__(self, containers, n_images, image_name_prefix, speed_factor,
                 power=10):
        super().__init__(
            containers, n_images, image_name_prefix, speed_factor,
            power, initial_x=1000
        )
        self.speed = -1


class Explosion(Sprite):
    DEFAULT_LIFE = 30

    def __init__(self, containers, actor):
        super().__init__(containers)
        img = utils.load_image(f"{ENTITY_PATH}explosion2.png")
        self.images = [pygame.transform.flip(img, True, True), img]
        self.image = self.images[1]
        self.rect = self.image.get_rect(center=actor.rect.center)

        self.life = self.DEFAULT_LIFE

    def update(self, _screen_rect):
        self.life -= 1
        self.image = self.images[self.life // 10 % 2]
        if self.life <= 0:
            self.kill()
