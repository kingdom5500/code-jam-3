# Stdlib
from threading import Thread
from typing import List, Type

# External Libraries
import pygame

# CJ3 Internals
from tenminutes.config import get_config
from tenminutes.game.level import Level
from tenminutes.game.scene import Scene, MainMenuScene


class Game:
    levels = []  # List[Level]
    entities = []  # List[SpawnableEntity]

    def __init__(self):
        self.config = None
        self.screen_rect = None
        self.screen = None
        self.scenes: List[Scene] = []
        self.running = False
        self.add_scene(MainMenuScene)

    def load_config(self):
        self.config = get_config()

    def add_scene(self, scene: Type[Scene]):
        s = scene(self)
        if isinstance(s, Level):
            Thread(target=s.start).start()
        self.scenes.append(s)

    def start(self):
        pygame.init()
        self.screen_rect = pygame.Rect(0, 0, *self.config["resolution"])
        self.screen = pygame.display.set_mode(self.screen_rect.size)
        self.running = True

        # TODO: Load save

        self._main_loop()
        pygame.quit()

    def _pop_last_scene(self):
        if self.scenes:
            last_scene = self.scenes.pop()
            last_scene.init()
            return last_scene

    def _main_loop(self):
        clock = pygame.time.Clock()
        active_scene = self._pop_last_scene()
        while self.running and active_scene is not None:

            if not active_scene.is_active:
                active_scene = self._pop_last_scene()

            if active_scene is None:
                break

            events, keys = self._get_inputs()
            active_scene.process_input(events, keys, self.screen_rect)
            active_scene.update(self.screen_rect)
            active_scene.render()

            pygame.display.flip()
            clock.tick(60)

    def _get_inputs(self):

        events = []
        pressed_keys = pygame.key.get_pressed()

        if pressed_keys[pygame.K_ESCAPE]:
            # TODO: Pause game if in a level
            # TODO: Save game
            self.running = False

        for event in pygame.event.get():
            quit_ = False

            if event.type == pygame.QUIT:
                quit_ = True

            if quit_:
                self.running = False
            else:
                events.append(event)

        return events, pressed_keys
